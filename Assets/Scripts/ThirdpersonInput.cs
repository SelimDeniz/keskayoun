using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ThirdpersonInput : MonoBehaviour
{

    public FixedJoystick LeftJoystick;
    public FixedButton Button;
    public FixedTouchField TouchField;
    protected PController Control;

    protected float CameraAngle;
    protected float CameraAngleSpeed = 0.2f;

    // Use this for initialization
    void Start()
    {
        Control = GetComponent<PController>();

    }

    // Update is called once per frame
    void Update()
    {
       // Control.JumpHeight = Button.Pressed;
       // Control.Hinput = LeftJoystick.inputVector.x;
       // Control.Vinput = LeftJoystick.inputVector.y;

        CameraAngle += TouchField.TouchDist.x * CameraAngleSpeed;

        Camera.main.transform.position = transform.position + Quaternion.AngleAxis(CameraAngle, Vector3.up) * new Vector3(0, 3, 4);
        Camera.main.transform.rotation = Quaternion.LookRotation(transform.position + Vector3.up * 2f - Camera.main.transform.position, Vector3.up);

    }
}

