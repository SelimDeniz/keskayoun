using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieAtack : MonoBehaviour
{
    //references 
    [SerializeField] private Animator animator;
    
    [SerializeField] private float damage;
    // Start is called before the first frame update
    void Start()
    {
       
        animator = GetComponentInParent<Animator>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag=="Player")
        Attack();
    }
    private void OnTriggerStay(Collider other)
    {
       // Attack();
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
            Move();
    }
    public void Attack()
    {

        Debug.Log("attack");
        animator.SetFloat("Blend", 1f);
      

    }
   
    public void Move()
    {
      //  Debug.Log("safe");
        animator.SetFloat("Blend", 0f);
    }
}

