using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trap : MonoBehaviour
{
    [SerializeField] private float cooldown;
    [SerializeField] private float tmr;
    private bool active = true;
    // Start is called before the first frame update
    private void Start()
    {
        transform.Find("cage").gameObject.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    { if (active == true)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                transform.Find("cage").gameObject.SetActive(true);
                active = false;
               StartCoroutine(DesCa(tmr));
                StartCoroutine(AcCa(cooldown+tmr));
                Debug.Log(active);
            }
        }
        
    }
    IEnumerator DesCa(float DelayTime)
    {
        yield return new WaitForSeconds(DelayTime);
        transform.Find("cage").gameObject.SetActive(false);
    }
    IEnumerator AcCa(float DelayTime)
    {
        yield return new WaitForSeconds(DelayTime);
        active = true;
    }
}
