using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ShrinkSkill", menuName = "Scr�ptableObjects/Skill/Shrink", order = 4)]
public class ShrinkSkill : Skill
{
    [SerializeField] public float shrinkScale = 10; //the times the player will be shrunk
    public override void Activate(GameObject parent)
    {
        //dividing the player scale value by the magnitude of the shrink effect
        Vector3 pScale = parent.transform.localScale;
        parent.transform.localScale = new Vector3(pScale.x / shrinkScale, pScale.y / shrinkScale, pScale.z / shrinkScale);
        parent.GetComponent<SkillHolder>().effect = SkillHolder.playerEffects.shrunk;
    }

    public override void Deactivate(GameObject parent)
    {
        //reversing the shrink operation
        Vector3 pScale = parent.transform.localScale;
        parent.transform.localScale = new Vector3(pScale.x * shrinkScale, pScale.y * shrinkScale, pScale.z * shrinkScale);
        parent.GetComponent<SkillHolder>().effect = SkillHolder.playerEffects.noEffect;
    }
}
