using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldBehaviour : MonoBehaviour
{
    //speed multiplier for ball after impact
    [SerializeField] public int reflectionPower;

    //on collision with ball reflect it with force
    private void OnCollisionEnter(Collision col)
    {
        Debug.Log("collision with" + col.gameObject.name);
        var tr = col.transform;
        if (tr.gameObject.CompareTag("Ball"))
        {
            tr.GetComponent<Rigidbody>().velocity = reflectionPower * tr.GetComponent<Rigidbody>().velocity.magnitude * transform.forward;
        }
    }
}
