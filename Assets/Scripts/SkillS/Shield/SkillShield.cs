using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;


[CreateAssetMenu(fileName = "SkillShield", menuName = "Scr�ptableObjects/Skill/Shield", order = 1)]
public class SkillShield : Skill
{
    [SerializeField] protected GameObject prefab; //prefab of the physical shield, also holds the oncollision function
    [SerializeField] private float spawnDist = 1f; //distance in front of player
    //[SerializeField] static string skillName = "Shield";
    GameObject player;
    private GameObject g;

    //public override string getSkill()
    //{
    //    return this.getSkillName();
    //}

    //setting the position of the shield and then spawning it
    public override void Activate(GameObject parent)
    {
        Debug.Log("rani nekhdem");
        Vector3 playerPos = parent.transform.position;
    //    Vector3 playerDir = parent.transform.forward * spawnDist;
        //Vector3 playerDir = parent.transform.position;
        Quaternion playerRot = parent.transform.rotation;

        Vector3 spawnPos = playerPos;
        spawnPos.y -= 7f;

        //setting the shield as child of player so it follows
        g = Instantiate(prefab, spawnPos, playerRot,parent.transform);
    }

    //destroying the shield
    public override void Deactivate(GameObject parent)
    {
        Destroy(g);
    }
}
