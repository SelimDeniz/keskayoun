using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.WSA;

[CreateAssetMenu(fileName = "MagnetSkill", menuName = "Scr�ptableObjects/Skill/Magnet", order = 2)]
public class MagnetSkill : Skill
{
    [SerializeField] private GameObject magnetPrefab;
    [SerializeField] private float magnetForce;
    [SerializeField] private float magnetDistance;
    private GameObject magnet;

    public override void Activate(GameObject parent)
    {
        magnet = Instantiate(magnetPrefab,parent.transform);
        magnet.GetComponent<MagnetPoint>().forceFactor = magnetForce;
        magnet.GetComponent<SphereCollider>().radius = magnetDistance;
    }

    public override void Deactivate(GameObject parent)
    {
        Destroy(magnet);
    }
}
