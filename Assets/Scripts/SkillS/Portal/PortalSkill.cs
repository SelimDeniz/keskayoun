using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
//using Unity.Burst.CompilerServices;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

[CreateAssetMenu(fileName = "PortalSkill", menuName = "Scr�ptableObjects/Skill/Portal", order = 2)]
public class PortalSkill : Skill
{
    [SerializeField] private float portalDistance = 50;
    [SerializeField] private GameObject portalPrefab;
    private GameObject p1, p2;
    public override void Activate(GameObject parent)
    {
        Debug.Log("Activating Portal" + parent.name);
        Ray ray = new Ray(parent.transform.position, parent.transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, portalDistance, LayerMask.GetMask("Ground","Hard Obstacle")))
        {
            spawnPortal(parent, hit.point);
        }
        else
        {
            spawnPortal(parent, parent.transform.forward * portalDistance);
        }
    }

    private void spawnPortal(GameObject parent, Vector3 target)
    {
        p1 = Instantiate(portalPrefab, target + parent.transform.up * 2, parent.transform.rotation);
        p1.name = parent.name + "'s FarPortal";
        p2 = Instantiate(portalPrefab, parent.transform.position + parent.transform.up * 2, parent.transform.rotation);
        p2.name = parent.name + "'s ClosePortal";

        p1.GetComponent<stepthoughportal>().SetTargetPortal(p2);
        p2.GetComponent<stepthoughportal>().SetTargetPortal(p1);
    }

    public override void Deactivate(GameObject parent)
    {
        Destroy(p1);
        Destroy(p2);
    }
}
