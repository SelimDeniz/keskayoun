using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stepthoughportal : MonoBehaviour
{
    [SerializeField]
    public GameObject otherPortal;
    public float timer;

    private void Update()
    {
        if (timer > 0)
        {
            gameObject.GetComponent<BoxCollider>().enabled = false;
            timer -= Time.deltaTime;
        }
        else
        {
            gameObject.GetComponent<BoxCollider>().enabled = true;
        }
    }

    void OnTriggerEnter(Collider other )
    {
        Debug.Log ("something hit the  portal"); 
          
        if (other.CompareTag("Player"))
        {
            otherPortal.GetComponent<stepthoughportal>().timer = 2;
            other.GetComponent<CharacterController>().Move(otherPortal.transform.position - other.transform.position);
        }
    }

    public void SetTargetPortal(GameObject targetPortal)
    {
        otherPortal = targetPortal;
    }

}

