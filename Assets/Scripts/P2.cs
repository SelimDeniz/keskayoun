using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.Antlr3.Runtime.Tree;
using UnityEngine;
using UnityEngine.UI;

public class P2 : MonoBehaviour
{

   
    public Rigidbody rb;
    public LayerMask LayerMask;
    public bool ground;
    public float Speed;

    // public float JumpSpeed;

    public FixedJoystick Leftjoystick;
    public FixedJoystick RightJoystick;
    public Button btn;

    float smooth = 5.0f;
    float tiltAngle = 60.0f;




    // Start is called before the first frame update
    void Start()
    {
        btn.onClick.AddListener(JumPingClick);
        rb = GetComponent<Rigidbody>();
        void JumPingClick()
        {
            
            

                // rb.AddForce(0, JumpSpeed * Time.deltaTime, 0);
                rb.AddForce(Vector3.up * 4, ForceMode.Impulse);
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        Move();
        
        Jump();

    }

    //  FixedUpdate can run once, zero, or several times per frame
    void FixedUpdate()
    {

    }
    private void Jump()
    {

        if ((Input.GetKeyDown(KeyCode.Space)) && ground)
        {

            // rb.AddForce(0, JumpSpeed * Time.deltaTime, 0);
            rb.AddForce(Vector3.up * 4, ForceMode.Impulse);
        }

    }

    

    private void Move()
    {
        //float verticalAxis;
        //float horizontalAxis;
        //if (Leftjoystick.Vertical == 0 || Leftjoystick.Horizontal == 0) { 

        //     verticalAxis = Leftjoystick.Vertical; //Input.GetAxis("Vertical");
        //     horizontalAxis = Leftjoystick.Horizontal;   // Input.GetAxis("Horizontal");

        //}
        //else
        //{
        //     verticalAxis =  Input.GetAxis("Vertical");
        //    horizontalAxis =   Input.GetAxis("Horizontal");
        //}

        float verticalAxis = Leftjoystick.Vertical; //Input.GetAxis("Vertical");
        float horizontalAxis = Leftjoystick.Horizontal;   // Input.GetAxis("Horizontal");

        Vector3 mv = transform.forward * verticalAxis + transform.right * horizontalAxis;
        mv.Normalize();
        transform.position += mv * 0.008f;

       
    }
    private void Rotate()
    {
        {
            // Smoothly tilts a transform towards a target rotation.
            float tiltAroundZ = RightJoystick.Horizontal * tiltAngle;
            //loat tiltAroundX = RightJoystick.Vertical * tiltAngle;

            // Rotate the cube by converting the angles into a quaternion.
            Quaternion target = Quaternion.Euler(0, tiltAroundZ, 0);

            // Dampen towards the target rotation
            transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);
        }
    }
}