using UnityEngine;
using System;
using Random = UnityEngine.Random;
using System.Collections.Generic;

public class KesKayounMatchEvents : MonoBehaviour
{
    [SerializeField] public Transform[] attackerSpawnPoints; //must be more than attackers
    private bool[] attackerSpawnPointsAvailability;
    [SerializeField] public Transform[] defenderSpawnPoints; //must be more than defenders
    private bool[] defenderSpawnPointsAvailability;
    [SerializeField] public Transform[] trapSpawnPoints;
    private bool[] trapSpawnPointsAvailability;
    [SerializeField] public Transform[] rockSpawnPoints;
    private bool[] rockSpawnPointsAvailability;

    [SerializeField] public GameObject[] attackers;
    [SerializeField] public GameObject[] defenders;
    [SerializeField] public GameObject rockPrefab;

    [SerializeField]
    public float gameTime = 90;

    [SerializeField]
    public float addedTime = 150;

    [SerializeField]
    public float currentTime = 0;

    private float refreshTime = 0;

    //Events
    public static event Action OnTimerRunningOut;
    public static event Action OnAllAttckersDead;
    public static event Action OnGameStart;
    public static event Action OnDefendersWinning;
    public static event Action OnAttackersinning;

    private void Start()
    {
        
        //Subscribe functions to events
        //Call Functions when event is Invoked
        //Event is invoked in PileManager
        PileManager.OnPileBreaking += ChangeBallHandlingRights;
        PileManager.OnPileBreaking += SpawnRocks;
        PileManager.OnPileBreaking += AddToTime;

        //event is invoked in this class
        OnTimerRunningOut += DefendersWin;
        OnTimerRunningOut += TimeRanOut;

        OnAllAttckersDead += DefendersWin;
        OnAllAttckersDead += AllAttackersDead;

        //PlayerHealth.OnPlayerDeath += CheckAttackersLives;

        PileManager.OnPileBuilt += AttackersWin;

        OnGameStart += SpawnPlayers;

        OnGameStart?.Invoke(); // Announce start of game
    }

    private void Update()
    {
        currentTime += Time.deltaTime;
        refreshTime += Time.deltaTime;
        if (currentTime > gameTime) OnTimerRunningOut?.Invoke(); //Announce Time Over
        if (refreshTime > 5)
        {
            Debug.Log("Game Time: " + currentTime + " / " + gameTime);
            refreshTime = 0;
        }

        //can Add an event that invokes when a player is eliminated
        //then call this function to check if that was the last player alive
        //to reduce for loops
    }

    private void OnDestroy()
    {
        //Unsubscribe Functions from events her
        PileManager.OnPileBreaking -= ChangeBallHandlingRights;
        PileManager.OnPileBreaking -= SpawnRocks;
        PileManager.OnPileBreaking -= AddToTime;

        OnTimerRunningOut -= DefendersWin;
        OnTimerRunningOut -= TimeRanOut;

        OnAllAttckersDead -= DefendersWin;
        OnAllAttckersDead -= AllAttackersDead;

        //PlayerHealth.OnPlayerDeath -= CheckAttackersLives;

        PileManager.OnPileBuilt -= AttackersWin;
    }

    //spawns players
    private void SpawnPlayers()
    {
        SpawnObjects(attackers, attackerSpawnPoints, attackerSpawnPointsAvailability);
        SpawnObjects(defenders, defenderSpawnPoints, defenderSpawnPointsAvailability);
    }

    //spawn 7 rocks on random spawn Points
    private void SpawnRocks()
    {
        Debug.Log("spawning Rocks");
        SpawnObjXTimes(rockPrefab, 7, rockSpawnPoints, rockSpawnPointsAvailability);
    }

    //spawns a list objects on random spawnPoints
    private void SpawnObjects(GameObject[] objects, Transform[] spawnPoints, bool[] spawnPointAvailability)
    {
        if (objects.Length > spawnPoints.Length)
        {
            Debug.Log("not Enough spawn Points for objects");
            return;
        }
        spawnPointAvailability = makeItTrue(spawnPoints.Length);
        int remainingPoints = spawnPointAvailability.Length; //for indexing remaining slots

        foreach (GameObject obj in objects)
        {
            int i = Random.Range(0,remainingPoints); //pick a random remaining spawn point
            for (int j = 0; j < spawnPoints.Length; j++)
            {
                //count i times open slots then spawn
                if (spawnPointAvailability[j])// open slots
                {
                    if (i == 0)// spawn now
                    {
                        Instantiate(obj, spawnPoints[j].position, Quaternion.identity);
                        spawnPointAvailability[j] = false;
                        remainingPoints--;// remove the point from the indexing
                        break;// no need to continue loop after spawning
                    }
                    else i--;//i times
                }
            }
        }
    }

    // spawn obj X times...
    private void SpawnObjXTimes(GameObject obj, int times, Transform[] spawnPoints, bool[] spawnPointAvailability)
    {
        
        if (spawnPoints.Length < times)
        {
            Debug.Log("not Enough spawn Points for objects");
            return;
        }
        spawnPointAvailability = makeItTrue(spawnPoints.Length);
        int remainingPoints = spawnPointAvailability.Length; //for indexing remaining slots

        for(int rockNumber=0; rockNumber < times; rockNumber++)
        {
            int i = Random.Range(0, remainingPoints); //pick a random remaining spawn point
            for (int j = 0; j < spawnPoints.Length; j++)
            {
                //count i times open slots then spawn
                if (spawnPointAvailability[j])// open slots
                {
                    if (i == 0)// spawn now
                    {
                        Instantiate(obj, spawnPoints[j].position, Quaternion.identity);
                        spawnPointAvailability[j] = false;
                        remainingPoints--; // remove the point from the indexing
                        break;// no need to continue loop after spawning
                    }
                    else i--;//i times
                }
            }
        }
    }

    private void ChangeBallHandlingRights()
    {

        Debug.Log("changing ball rights");
        //TODO: remove ball controller from teamAttack
        //TODO: add ball controller to teamDefense
        //TODO: set canDieFromBallHit for each player

        foreach (GameObject player in attackers)
        {
            //player.GetComponent< /* ballController */ >().SetActive(player.GetComponent<PController>()./* hasBallRights */());
        }
    }

    private void AttackersWin()
    {
        Debug.Log("Attackers Win!");
        //TODO: Attackers Win
    }

    private void DefendersWin()
    {
        Debug.Log("Defenders Win!");
        //TODO: Defenders Win
    }

    private void TimeRanOut()
    {
        Debug.Log("Time Over!");
        //TODO Time Ran Out
    }

    private void AllAttackersDead()
    {
        Debug.Log("All Attackers Dead");
        //TODO All Attackers Dead
    }

    private void AddToTime()
    {
        gameTime = addedTime;
    }

    private void CheckAttackersLives()
    {
        //TODO: ckeckAttackersLives and implement call for player health component

        //int deadAttackersCount = 0;
        //foreach (GameObject player in attackers)
        //{
        //    if (player.GetComponent<playerHealth>().isDead())
        //    {
        //        deadAttackersCount++;
        //    }
        //}
        //if (deadAttackersCount == attackers.Length)
        //{
        //    OnAllAttckersDead?.Invoke();
        //}
    }

    //returns a list of true for initializing spawn points
    private bool[] makeItTrue(int l)
    {
        bool[] result = new bool[l];
        for (int i = 0; i < l; i++)
        {
            result[i] = true;
        }
        return result;
    }
}
