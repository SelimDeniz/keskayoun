using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//attached to rockdetector in player
public class rockDetector : MonoBehaviour
{
    [SerializeField] public int playerMaxRocks = 2; //How many rocks can the player carry at a time

    [SerializeField] private int playerRocks =  0; //How many rocks the player has currently

    //auto pick rocks close by
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Rock") && playerRocks < playerMaxRocks) // pickup only when has space
        {
            playerRocks++;
            Destroy(other.gameObject);

            //can add pickup animation here
        }
    }

    //number of rocks on player
    public int getPlayerRocks()
    {
        return playerRocks;
    }

    //use a rock
    public void useRock()
    {
        if (playerRocks> 0)
        {
            playerRocks -= 1;
        }
    }

    //check if player has rocks
    public bool playerHasRocks()
    {
        return (playerRocks > 0);
    }
}
