using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class rockBuilder : MonoBehaviour
{
    [SerializeField] KeyCode build = KeyCode.Space; //TODO convert to phone Button
    [SerializeField] float timeToBuild = 2; //time requiered to build a rock in seconds
    [SerializeField] private Button builds;
    [SerializeField] private bool isbuilding;
    float timer = 0;

    private rockDetector rd;

    private void Start()
    {
        builds.enabled = false;
        rd = transform.parent.Find("rockDetector").GetComponent<rockDetector>();
    }
    //if player is in range of the pile, they can build
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Pile"))
        {
            // player needs to have a rock in inventory
            if (rd.playerHasRocks())
            {
              //  builds.enabled = true;
                //TODO activate build button in HUD
              
                //player needs to hold down build button
                //if (Input.GetKey(build))//button to be changed
                    
                //{
                    
                    //TODO set building animation

                    if (timer >= timeToBuild)
                    {
                        //add rock to pile
                        //and remove a rock from inv
                        //and restart timer
                        if (other.GetComponent<PileManager>().addRockToPile())
                        {
                            rd.useRock();
                            timer = 0;
                        };
                    }
                    else
                    {
                        //TODO can show UI progress here
                        timer += Time.deltaTime;
                    }
                }
                //player releases button and timer resets
                //if (Input.GetKeyUp(build))//button to be changed
                //{
                //    //TODO reset UI progress
                //    timer = 0;
                //}
            //}
            else
            {
                builds.enabled = false ;
                //reset timer when player has no rocks
                timer = 0;

                //TODO And deactivate Build Button in HUD
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.CompareTag("Pile"))
            builds.enabled=false;
    }

}
